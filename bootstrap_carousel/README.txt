
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Credits


 INTRODUCTION
------------

bootstrap carousel is intended to be used with The Twitter bootstrap theme (http://drupal.org/project/twitter_bootstrap) version 7.x-2.0-beta1 and above, to help drupal sight creators leavrage 

Drupal 7 core to create a flexable bootstrap carousel with no knolage of code .


INSTALLATION
------------

1. Copy the bootstrap_carousel directory to your sites/SITENAME/modules directory.

2. Enable the module at Administer >> Site building >> Modules 3. view bootstrap_carousel module at Administer >> Site building >> Modules - bootstrap_carousel help 


CREDITS
-------
* Taggart Jensen (taggartj)